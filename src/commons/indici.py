'''
Created on 14/gen/2014

@author: knizontes
'''

from algorithms import Algorithms

class IndexesMaker():
    
    
    #locali
    #rhoI
    
        
    @staticmethod
    def GNRhoI(states, statesProbs, M):
        retval = []
        for j in range (M):
            rhoJ = 0
            for i in range (len(states)):
                if (states[i][j]>0):
                    rhoJ+=statesProbs[i]
            retval.append(rhoJ)
        return retval
    
    @staticmethod
    def MVARhoI(lambdas, mis):
        retval = []
        for i in range(len(mis)):
            retval.append( float(lambdas[i])/mis[i])
        return retval
        
    
    #popolazione media del centro
    
    
    #tempo medio di risposta del centro i
    #probabilità marginali
    
    #globali
    #tempo medio di risposta
    #tempo medio di ciclo
    #throughput di impianto visto da un centro
    
    
    
