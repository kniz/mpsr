#!/usr/bin/python
#coding: utf-8
'''
Created on 13/gen/2014

@author: knizontes
'''
import cPickle as pickle

from gordonNewell.gordonNewell import GordonNewell
from mva.mva import MVA

minSs = [1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 7, 7, 8, 8, 9, 10,\
         11, 12, 13, 14, 15, 16, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]

def gnRun():
    print 'Risoluzione del primo modello tramite Gordon Newell'
    s = raw_input('\t1. esecuzione standard\n\t2.stampa vettore probabilità in funzione del numero di job N\n[1,2]:')
    if s[0]=='1':
        s = raw_input('inserire N:')
        n = int(s)
        gn = GordonNewell(n)
        gn.solve()
        gn.printS()
        gn.printIndexes()
    else:
        probList = []
        for n in range(1,51):
            gn = GordonNewell(n)
            gn.solve(False)
            #gn.printS()
            #gn.printIndexes()
            probList.append(gn.getSProbsList())
#         print 'probList = %s'%(str(probList))
        print 'serializing...'
        out_pkl = open('probs.pkl','wb')
        pickle.dump(probList,out_pkl)
        print 'done!'
        
        

def MVARun():
    print 'Risoluzione del secondo modello tramite MVA'
    print 'ricerca delle soglie S1, S2 ottimali rispetto al tempo di risposta dell\'impianto visto dal centro client 1...'
    
    sMin = 31
    m = 6
    s = raw_input('inserire n:')
    n = int(s)
    
    s1 = 100
    s2 = 100
    eTrMin = 10000.0
    leastEtrMva = None
    mvas = []
    for i in range(sMin,n+1):
        for j in range(sMin,n+1):
            mvas.append(MVA(m,n,i,j))
            mvas[-1].solve()
            mvas[-1].localIndexesCalculate()
            mvas[-1].globalIndexesCalculate()
            eTr = mvas[-1].systemResponseTimeClient1_3()
            if eTr < eTrMin:
                eTrMin = eTr
                s1 = mvas[-1].getS1()
                s2 = mvas[-1].getS2()
                leastEtrMva = mvas[-1]
            #print 's1:%d s2:%d eTr0:%f eTr0Min:%f'%(i,j,eTr,eTrMin)
        #print 'il tempo di risposta minimo del sistema eTr=%f considerato è con le soglie:\n\ts1:%d\n\ts2:%d'%(eTrMin,s1,s2)
    #print '\tN=%d  min eTr:%f\ts1:%d\ts2:%d'%(n,eTrMin,s1,s2)
    
    leastEtrMva.printIndexes()
    leastEtrMva.printFirstIndexes()

def MVAThreasholdSearchRun(centerType):
    print 'Risoluzione del secondo modello tramite MVA'
    print 'opzione %d'%(centerType)
    print 'esecuzione dei possibili modelli al variare di N'
    
    #sMin = 31
    m = 6
    #n = 47
    #nMin = sMin
    nMax = 50
    responseTimesFile = open('response_times.csv','w')
    systemResponseTimesFile = open('sys_response_times.csv','w')
    shortSystemResponseTimesFile = open('sys_response_times_short.csv','w')
    sageFileS1S2 = open('S1S2_type%d.sage'%(centerType),'w')
    sageFileNS2 = open('NS2_type%d.sage'%(centerType),'w')
    sageFileNS1 = open('NS1_type%d.sage'%(centerType),'w')
    sageListS1S2 = []
    sageListNS2 = []
    sageListNS1 = []
    
    responseTimesFile.write('N,S1,S2,')
    systemResponseTimesFile.write('N,S1,S2,')
    shortSystemResponseTimesFile.write('N,S1,S2,')
    
    
    
    for i in range(m):
            responseTimesFile.write('center %d,'%(i+1))
            systemResponseTimesFile.write('center %d,'%(i+1))
    responseTimesFile.write('Client1 with rejection,\n')
    systemResponseTimesFile.write('Client1 with rejection,\n')
    shortSystemResponseTimesFile.write('Client1 with rejection,\n')
            
    for n in range(1,nMax+1):
        s1 = 100
        s2 = 100
        eTrMin = 10000.0

        mvas = []
        sMin = minSs[n-1]
        for i in range(sMin,n+1):
            mvas.append([])
            for j in range(sMin,n+1):
                mvas[i-sMin].append(MVA(m,n,i,j))
                mvas[i-sMin][-1].solve()
                mvas[i-sMin][-1].localIndexesCalculate()
                mvas[i-sMin][-1].globalIndexesCalculate()
                #eTr = mvas[i-sMin][-1].getResponseTimeAvg(0)
                if centerType==1:
                    eTr = mvas[i-sMin][-1].systemResponseTimeClient1_1()
                elif centerType==2:
                    eTr = mvas[i-sMin][-1].systemResponseTimeClient1_2()
                elif centerType==3:
                    eTr = mvas[i-sMin][-1].systemResponseTimeClient1_3()
                elif centerType==4:
                    eTr = mvas[i-sMin][-1].systemResponseTimeClient1_4()
                else:
                    eTr = mvas[i-sMin][-1].systemResponseTimeClient1_5()
                mvas[i-sMin][-1].responseTimesToFile(responseTimesFile,'%d,%d,%d,'%(n,i,j))
                mvas[i-sMin][-1].systemResponseTimesToFile(systemResponseTimesFile,'%d,%d,%d,'%(n,i,j))
                #mvas[i-sMin][-1].shortSystemResponseTimesToFile(shortSystemResponseTimesFile,'%d,%d,%d,'%(n,i,j))
                
                if n==50:
                    sageListS1S2.append((i,j,eTr))
                if i == 31:
                    sageListNS2.append((n+1,j,eTr))
                if j == 31:
                    sageListNS1.append((n+1,i,eTr))
                    
                if eTr < eTrMin:
                    eTrMin = eTr
                    s1 = mvas[i-sMin][-1].getS1()
                    s2 = mvas[i-sMin][-1].getS2()
                    leastEtrMva = mvas[i-sMin][-1]
                #print 's1:%d s2:%d eTr0:%f eTr0Min:%f'%(i,j,eTr,eTrMin)
        #print 'il tempo dsageListNS2.append(())i risposta minimo del sistema eTr=%f considerato è con le soglie:\n\ts1:%d\n\ts2:%d'%(eTrMin,s1,s2)
        sageFileS1S2.write(str(sageListS1S2))
        sageFileNS2.write(str(sageListNS2))
        sageFileNS1.write(str(sageListNS1))
        
        print '\tN=%d  min eTr:%f\ts1:%d\ts2:%d'%(n,eTrMin,s1,s2)

if __name__=="__main__":
    print '\n\n================================================='
    print '=\t Modelli di Prestazione di \t\t='
    print '=\t      Sistemi e Reti\t\t\t='
    print '=\t\t2012-2013\t\t\t='
    print '================================================='
    
    while True:
        s1 = raw_input('\n\nModello da risolvere:\n\t1. modello 1 (algoritmo Gordon-Newell)\
        \n\t2. modello 2 (algoritmo MVA semplice, parametri custom)\n\t3. modello 2 (algoritmo MVA, ricerca soglie minime al variare del numero di utenti)\n\n[1,2,3]:')
        if s1[0]=='1':
            gnRun()
                
        elif s1[0]=='2':
            MVARun()
        
        elif s1[0]=='3':
#             MVAThreasholdSearchRun(1)
#             print '--------------------------------------'
            MVAThreasholdSearchRun(4)
            print '--------------------------------------'
            MVAThreasholdSearchRun(5)
        
        elif s1=='exit' or s1=='quit' :
            print 'bye!'
            break
        else:
            print 'inserire 1, 2, 3 o exit'
    