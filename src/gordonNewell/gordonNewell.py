#! /usr/bin/python
#coding: utf-8 
'''
Created on 13/gen/2014

@author: knizontes
'''

import datetime
from math import factorial

class GordonNewell():
    
    
    
    IS = [True,False,False,True]
    xs = [1.0, 0.892857108, 0.238095229, 19.83333256 ]
    ts = [7.0, 0.3, 0.08, 7.0]
    responseTimeAvgs = None
    cycleTimeAvg = None
    throughput = None
    sProbBound = 0.78
    s = None
    sProb = None
    
    
    def __init__(self, N=50,M=4):
        self.N = N
        self.M = M
        self.vs=[]
        self.mu = []
        self.produttorie = []
        self.normalizationCostant = 0
        self.stateProbabilities = []
        self.sum=0
        self.lambdas = []
        self.rhos = []
        self.nLocalAvg = []
        self.executionTimeAvgs = []
        self.array = []
        self.states = []
        self.sProbs = []
        self.marginalProbabilities = []
        self.responseTimeAvg = 0
        for i in range(M):
            self.array.append(0)
            self.mu.append(1.0/self.ts[i])
            
    
    def generaStati(self, index, jobNum):
        for i in reversed(range(jobNum+1)):
            tmpJobN = jobNum -i
            self.array[index]=i
            if (tmpJobN > 0) and ( (index+1) < self.M) :
                self.generaStati(index+1, tmpJobN)
            else:
                for j in range (index+1, len(self.array)):
                    self.array[j]=0
                if tmpJobN == 0:
                    self.addState();
    
    def addState(self):
        tmpArray = []
        for i in range (self.M ):
            tmpArray.append(self.array[i])
        self.states.append(tmpArray)
    
    def printStates(self):
        for i in range(len(self.states)):
            print '%d. %s'%(i,str(self.states[i]))
            
    def produttorieInit(self):
        for i in range(len( self.states)):
            tmpProd = 1
            for j in range (self.M):
                tmpExp = self.xs[j]**self.states[i][j]
                if self.IS[j]:
                    tmpProd*= float(tmpExp)/factorial(self.states[i][j])
                else:
                    tmpProd*= tmpExp
                
                
            self.produttorie.append(tmpProd)
            
            
            
    
    def normalizationCostantCalculate(self):
        for produttoria in self.produttorie:
            self.normalizationCostant+=produttoria
    
    
    def stateProbabilitiesCalculate(self):
        self.sum =0.0
        for produttoria in self.produttorie:
            self.stateProbabilities.append( float(produttoria) / self.normalizationCostant )
            self.sum+=self.stateProbabilities[-1]
            if self.stateProbabilities[-1] > 1:
                print 'ops!'
    
    def debug(self):
        
        for i in range (50):
            print self.stateProbabilities[-50+i]
        print '\nsum:%f'%(self.sum)
             
    
    def makeCsv(self):
        path='data.csv'
        f=open(path,"w")
        f.write('state, probability,\n')
        for i in range(len(self.states)):
            f.write('%s,%s,\n'%(str(self.states[i]).replace(',',';'), str(self.stateProbabilities[i])))
        f.close()
        
        
    #local indexes


    def rhosCalculate(self):
        for j in range (self.M):
            rhoJ = 0
            for i in range (len(self.states)):
                if (self.states[i][j]>0):
                    rhoJ+=self.stateProbabilities[i]
            self.rhos.append(rhoJ)
        
    
    def lambdasCalculate(self):
        for i in range(self.M):
            self.lambdas.append( self.rhos[i]* self.mu[i])
    
    def nLocalAvgCalculate(self):
        for j in range(self.M):
            self.nLocalAvg.append(0)
            for i in range(len(self.states)):
                self.nLocalAvg[-1] += self.states[i][j]*self.stateProbabilities[i]
                
    def executionTimeLocalAvg(self):
        for j in range (self.M):
            if self.IS[j]:
                self.executionTimeAvgs.append( self.ts[j] )
            else:
                self.executionTimeAvgs.append( float(self.nLocalAvg[j])/self.lambdas[j] )
            
    def responseTimeLocalAvg(self):
        self.vsCalculate()
        self.responseTimeAvgCalculate()
    
    def marginalProbabilitiesCalculate(self):
        for j in range (self.M):
            self.marginalProbabilities.append([])
            for i in range (self.N+1):
                self.marginalProbabilities[-1].append(0)
        for i in range (len (self.states)):
            for j in range(self.M):
                tmp=self.states[i][j]
                if tmp > 0:
                    self.marginalProbabilities[j][tmp]+= self.stateProbabilities[i]
    
    def vsCalculate(self):
        for i in range(self.M):
            self.vs.append([])
            for j in range (self.M):
                self.vs[i].append(float(self.lambdas[i])/self.lambdas[j])
    
    def responseTimeAvgCalculate(self):
        self.responseTimeAvgs = []
        for j in range (self.M):
            self.responseTimeAvgs.append(0)
            for i in range (self.M):
                if i == j:
                    continue
                self.responseTimeAvgs[j]+= self.vs[i][j]*self.executionTimeAvgs[i]
                
    # global indexes
    def responseTimeAvgGlobalCalculate(self):
        # we use the first center as reference
        for i in range (1,self.M):
            self.responseTimeAvg += self.vs[i][1] * self.responseTimeAvgs[i]
    
    def cycleTimeAvgGlobalCalculate(self):
        self.cycleTimeAvg = self.responseTimeAvg + self.responseTimeAvgs[0]
    
    def throughputCalculate(self):
        self.throughput = float(self.N)/ self.cycleTimeAvg
        
    def findLowerS(self):
        for i in range (self.N+1):
            self.sProbs.append(0)
        for i in range(len(self.states)):
            tmp = self.states[i][1]+self.states[i][2]
            self.sProbs[tmp]+=self.stateProbabilities[i]
        tmpSum =0.0
        for i in range(self.N+1):
            if tmpSum >self.sProbBound:
                self.s=i
                self.sProb = 1.0-tmpSum
                break
            tmpSum += self.sProbs[i]
            
    def getS(self):
        return self.s
    
    def getSProb(self):
        return self.sProb
    
    def printS(self):
        tmpSum=0.0
        print 'soglie:'
        for i in range(self.N+1):
            print '\t%d. %0.20f'%(i,1.0-tmpSum)
            tmpSum+=self.sProbs[i]
        print 'La soglia s è %d'%(self.s)
        print 'costante norm:%0.20f'%(self.normalizationCostant)
    
    def localIndexesCalculate(self):
        self.rhosCalculate()
        self.lambdasCalculate()
        self.nLocalAvgCalculate()
        self.executionTimeLocalAvg()
        self.responseTimeLocalAvg()
        #self.marginalProbabilitiesCalculate()
        
    def globalIndexesCalculate(self):
        self.responseTimeAvgGlobalCalculate()
        self.cycleTimeAvgGlobalCalculate()
        self.throughputCalculate()
        
    def printSProb(self):
        tmpSum = 0.0
        probs = []
        for i in range(self.N+1):
            probs.append(1.0-tmpSum)
            tmpSum+=self.sProbs[i]
        print probs
    
    def getSProbsList(self):
        tmpSum = 0.0
        probs = []
        for i in range(self.N+1):
            probs.append(1.0-tmpSum)
            tmpSum+=self.sProbs[i]
        return probs
        
        
    def printIndexes(self):
        print '============================================='
        print '=\t\tLOCAL INDEXES\t\t='
        print '============================================='
        for i in range (self.M):
            print '\ncenter %d:\n\tmu:\t\t\t%0.20f\n\tlambda:\t\t\t%0.20f\n\tutilizationFactor:\t%0.20f\n\tEn:\t\
            \t%0.20f\n\tEtr:\t\t\t%0.20f\n\tEt:\t\t\t%0.20f\n\trho:\t\t\t%0.20f\n'%(i+1,self.mu[i],self.lambdas[i],self.rhos[i],\
                                                     self.nLocalAvg[i],self.responseTimeAvgs[i],self.executionTimeAvgs[i],self.rhos[i])
        print '============================================='
        print '=\t\tGLOBAL INDEXES\t\t='
        print '============================================='
        print 'System Response Time:%0.20f\nSystem Cycle Time:%0.20f\nSystem Troughput:%0.20f'%\
        (self.responseTimeAvg,self.cycleTimeAvg,self.throughput)    
        
    def solve(self, verbose=True):
        if verbose:
            print 'generate states...'
            self.generaStati(0, self.N)
            print 'done.'
            print 'calculate state probailities...'
            self.produttorieInit()
            self.normalizationCostantCalculate()
            self.stateProbabilitiesCalculate()
            print 'done.'
            print 'find lower bound for S (useful for next exercise)...'
            self.findLowerS()
            print 'done'
            print 'calculating local indexes...'
            self.localIndexesCalculate()
            print 'done.'
            print 'calculating global indexes...'
            self.globalIndexesCalculate()
            print 'done'
        else:
            self.generaStati(0, self.N)
            self.produttorieInit()
            self.normalizationCostantCalculate()
            self.stateProbabilitiesCalculate()
            self.findLowerS()
            self.localIndexesCalculate()
            self.globalIndexesCalculate()
        
    
if __name__=="__main__":
    print 'Gordon Newell !!!'
    
    m = 4
    while True:
        print '--------------------------------------------'
        s = raw_input('\t1. Gordon Newell\n\t2. Print thresholds array\n\t[1,2]:')
        if s[0] == '1':
            s = raw_input('inserire n:')
            n = int(s)
            gn = GordonNewell(n,m)
            gn.solve()
            gn.printS()
            gn.printIndexes()
            print 'done!'
        elif s[0] =='2':
            #the array of minimum thresholds
            ss = []
            for n in range(1,51):
                print 'solving Gordon Newell algorithm for n:%d...'%(n),
                gn = GordonNewell(n,m)
                gn.solve(False)
                ss.append(gn.getS())
                print ' done!'
            print 'ss: %s'%(str(ss))
        else:
            print 'bye!'
            break
                
    #gn.printSProb()
    
    #gn.printStates()
    